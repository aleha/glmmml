
<!-- Readme.md is generated from Readme.org. Please edit that file -->


# glmmml R package

This package provides functionality to combine machine learning
methods with random effects.

The
idea is to iterate in the training between a (empty) random effects
model and a fixed effect machine learning model:

![img](README-glmmml_scheme.png)

The code is adapted
from \code{\link[glmertree]{glmertree}}, the algorithm follows Ngufor
et al. (2019; 10.1016/j.jbi.2018.09.001).

  The idea follows
Ngufor et al., 2019, Journal of Biomedical Informatics ([doi:10.1016/j.jbi.2018.09.001](https://doi.org/10.1016/j.jbi.2018.09.001))
the implementation draws from [glmertree](https://cran.r-project.org/package=glmertree).

For help to set it up with your machine learning algorithm, please
look at `glm_wrapper.R` and `cforest_wrapper.R`.


## Installation

Installation directly from gitlab is possible, when the package
[devtools](https://cran.r-project.org/package=devtools) is installed.

You can install this package with

```R
library("devtools")
install_git("https://gitlab.gwdg.de/aleha/glmmml")
```


## Roadmap

Next steps include:

-   support for easy handling of regression (linear mixed effect models)

<!--
Local Variables:
 mode: gfm
 markdown-command: "marked"
End:
-->
